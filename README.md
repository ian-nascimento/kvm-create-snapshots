kvm-create-snapshots
============================

Programa que cria snapshots das máquinas virtuais kvm que forem informadas por parâmetro e
foi inicialmente pensado para ser utilizado em conjunto com o crontab.

A ideia é que o usuário tenha facilidade em agendar a frequência com que os snapshots são gerados.


Descrição do funcionamento
--------------------------

*1* - O programa recebe como parâmetro o nome da máquina virtual através da opção `--virtual-machine`  

*2* - No arquivo crontab (`/etc/crontab`) um cronjob com a definição dos intervalos específicos de cada máquina virtual é criado.

*3* - Se o snapshot não existir, então o mesmo é criado tendo como seu nome a data no formato `yyyymmdd`.

*4* - Se houver algum erro no momento em que o snapshot for criado, então uma mensagem com a desccrição do erro é enviada para o canal no RocketChat informado no script.

Exemplo de cronjobs
-------------------
```
# Gera os snapshot das VMs "kali-linux-01"  e "debian10-lab-01" às 01:00 em todo primeiro e segundo domingo do mês, respectivamente.
00 01 1-7 * *    root   test `/bin/date +\%w` -eq 0 && /opt/kvm-create-internal-snapshot/kvm-create-internal-snapshot.sh --virtual-machine kali-linux-01
00 01 8-14 * *   root   test `/bin/date +\%w` -eq 0 && /opt/kvm-create-internal-snapshot/kvm-create-internal-snapshot.sh --virtual-machine debian10-lab-01
```
